"""
_version.py

This module simply defines the version of the package.

The version string is stored in the __version__ variable. This module is typically used in
setup.py or other parts of the package where the version number needs to be accessed or
displayed. Following Semantic Versioning guidelines, the version string is expected to be
in the format 'MAJOR.MINOR.PATCH'.

Attributes:
    __version__ (str): A string representing the current version of the package.
"""

__version__ = "0.1.0"
