.. Google Text-To-Speech documentation master file, created by
   sphinx-quickstart on Mon Dec 11 11:29:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Google Text-To-Speech's documentation!
=================================================

Contents
--------

.. toctree::
   :maxdepth: 2

   src/user_stories
   src/architecture

