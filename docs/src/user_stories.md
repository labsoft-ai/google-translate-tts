# User Stories

1. **User Story: Basic Usage**

   * **As a** language learner,
   * **I want** to hear how sentences are pronounced in different languages,
   * **So that** I can improve my pronunciation skills.

   **Acceptance Test:**

   * Given a sentence in a foreign language and its language code,
   * When I use the `play_tts` function,
   * Then I should hear the sentence pronounced correctly in the specified language.

   *Not fully testable through automated unit tests.* This user story requires manual testing to verify the quality and correctness of the audio output in different languages.
2. **User Story: Long Text**

   * **As a** user reading long articles,
   * **I want** to listen to the article in chunks,
   * **So that** I can understand the content without the system crashing or the text being cut off.

   [**Acceptance Test:**](../../tests/test_google_translate_tts.py)

   * Given a long text that exceeds typical URL length limits,
   * When I use the `play_tts` function,
   * Then the text should be broken into sentences and each sentence should be played in sequence.
3. **User Story: Accessibility**

   * **As a** person with visual impairments,
   * **I want** to use a TTS function to read out digital text content,
   * **So that** I can access information easily.

   **Acceptance Test:**

   * Given a variety of texts from different sources,
   * When I use the `play_tts` function,
   * Then I should be able to hear the text clearly and in the correct order.

   *Not fully testable through automated unit tests.* This story primarily pertains to the audio output's accessibility and clarity, which would require manual testing. Automated tests can ensure that the text is processed, but not how it's audibly presented.
4. [**User Story: Language Support**](../../tests/test_google_translate_tts.py)

   * **As a** multilingual user,
   * **I want** to hear texts in different languages,
   * **So that** I can use the tool for various language contents.

   **Acceptance Test:**

   * Given texts in different languages with the correct ISO language codes,
   * When I use the `play_tts` function,
   * Then each text should be played in its respective language.



## Additional Notes:

* The current automated tests focus on the text processing and request generation aspects of the `play_tts` function.
* The audio output, which is central to the user stories, still requires manual testing to fully ensure that the user's needs are met, especially regarding pronunciation quality and language accuracy.
* Further automated tests could be developed if more functionalities are decomposed into testable units, such as error handling, response validation, and network request success.
