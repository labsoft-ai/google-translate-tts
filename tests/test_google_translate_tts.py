"""
Unit tests for the Google Text to Speech functionalities.

This module contains a series of unit tests for testing the functions in the
google_text_to_speech.google_translate_tts module. It includes tests for URL generation, text splitting,
and the play_tts function. These tests primarily use the unittest framework and mock key
functionalities to isolate and test specific components of the text-to-speech functionality.

The tests are structured to run with a test runner that can generate JUnit XML reports,
which are useful for integrating the test results into continuous integration (CI) systems.

Functions:
- test_generate_url: Tests the URL generation for TTS requests.
- test_splitting_long_sentence: Tests splitting a long sentence into smaller parts.
- test_splitting_text: Tests splitting text into sentences and smaller parts if necessary.
- test_play_tts: Tests the play_tts function with mocked external calls.

The module can be run directly as a script, which will generate a test report in the specified directory.
"""

import os
import unittest
from unittest.mock import patch, Mock
import xmlrunner

from google_text_to_speech.google_translate_tts import (
    play_tts,
    generate_url,
    split_long_sentence,
    split_text,
)


class TestGoogleTextToSpeech(unittest.TestCase):
    """Tests for Google Text to Speech functions."""

    def test_generate_url(self):
        """Test URL generation for TTS request."""
        text = "Hello"
        lang = "en"
        expected_url = "https://translate.google.com/translate_tts?ie=UTF-8&tl=en&client=tw-ob&q=Hello"
        url = generate_url(text, lang)
        self.assertEqual(url, expected_url)

    def test_splitting_long_sentence(self):
        """Test splitting a long sentence into smaller parts."""
        sentence = (
            "This is a long sentence, which needs to be split, because it is too long."
        )
        expected_parts = [
            "This is a long sentence,",
            "which needs to be split,",
            "because it is too long.",
        ]
        result = split_long_sentence(sentence, max_length=30)
        self.assertEqual(result, expected_parts)

    def test_splitting_text(self):
        """Test splitting text into sentences and then into smaller parts if necessary."""
        text = "This is a sentence. And another one! Is this the third one? Yes, it is."
        expected_sentences = [
            "This is a sentence.",
            "And another one!",
            "Is this the third one?",
            "Yes, it is.",
        ]
        sentences = split_text(text, max_length=25)
        self.assertEqual(sentences, expected_sentences)

    @patch("google_text_to_speech.google_translate_tts.requests.get")
    @patch("google_text_to_speech.google_translate_tts.playsound")
    @patch("google_text_to_speech.google_translate_tts.tempfile.NamedTemporaryFile")
    @patch("google_text_to_speech.google_translate_tts.os.remove")
    def test_play_tts(self, mock_remove, mock_tempfile, mock_playsound, mock_get):
        """Test the play_tts function with mocked external calls."""
        # Set up mock response for requests.get
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.content = b"fake audio data"
        mock_get.return_value = mock_response

        # Mock the tempfile to simulate file creation
        mock_tempfile.return_value.__enter__.return_value.name = "tempfile.mp3"

        text = "Test text"
        lang = "en"

        play_tts(text, lang)

        mock_get.assert_called_once()
        mock_playsound.assert_called_once_with("tempfile.mp3")
        mock_remove.assert_called_once_with("tempfile.mp3")

    # Additional tests can be added here for more coverage


if __name__ == "__main__":
    # Define the directory where the JUnit XML report will be saved
    REPORT_DIR = "tests/test-reports"
    # Ensure the directory exists
    os.makedirs(REPORT_DIR, exist_ok=True)

    # Run the tests with xmlrunner as the test runner
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output=REPORT_DIR),
        failfast=False,
        buffer=False,
        catchbreak=False,
    )
