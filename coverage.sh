#!/bin/bash
# Script to run tests with coverage and generate a report

# Define the desired coverage threshold
COVERAGE_THRESHOLD=80

# Running tests with coverage
# Ensure to replace 'tests/test_google_translate_tts.py' with your specific test script if different
coverage run --source=src tests/test_google_translate_tts.py

# Generating a coverage report in XML format (for CI tools like GitLab)
coverage xml -o coverage.xml

# Generating a terminal report for coverage and capturing the output
COVERAGE_OUTPUT=$(coverage report)

# Echo coverage output to the terminal
echo "$COVERAGE_OUTPUT"

# Optional: Generate an HTML report for detailed coverage review
coverage html -d public/coverage_html_report

# Extracting the total coverage percentage
COVERAGE_PERCENT=$(echo "$COVERAGE_OUTPUT" | grep 'TOTAL' | awk '{print $NF}' | sed 's/%//')

# Check if the coverage is below the threshold
if [[ "$COVERAGE_PERCENT" -lt "$COVERAGE_THRESHOLD" ]]; then
    echo "Coverage threshold of $COVERAGE_THRESHOLD% not met. Coverage is $COVERAGE_PERCENT%."
    exit 1
else
    echo "Coverage threshold of $COVERAGE_THRESHOLD% met. Coverage is $COVERAGE_PERCENT%."
fi
