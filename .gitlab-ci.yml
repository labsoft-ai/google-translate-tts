image: python:3.8

stages:
  - test
  - lint
  - build-docs
  - deploy


spellcheck:
  stage: test
  image: node:latest
  script:
    - npm install -g markdown-spellcheck@0.11.0 
    - mdspell "**/*.md" --en-us --ignore-numbers --ignore-acronyms --ignore .spelling --report
linkcheck:
  stage: test
  image: node:latest
  script:
    - npm install -g markdown-link-check
    - find . -name \*.md -exec markdown-link-check {} \;


run_tests:
  stage: test
  before_script:
    - pip install -e . # Install your package in editable mode
    - pip install -r requirements.txt # Install dependencies
  script:
    - ./coverage.sh # Discover and run tests in the tests directory, and publish the test and coverage reports
  artifacts:
    paths:
      - tests/test-reports/*.xml
      - coverage.xml
      - public/coverage_html_report/
    reports:
      junit: tests/test-reports/*.xml
    when: always

pylint:
  stage: lint
  before_script:
    - pip install -e . # Install your package in editable mode
    - pip install -r requirements.txt # Install dependencies
    - pip install pylint pylint-gitlab
    - mkdir -p public/lint public/badges # Create necessary directories
  script:
    # Define a variable for python files excluding .venv directory
    - PY_FILES=$(find . -type f -name "*.py" ! -path "./.venv/*")

    # Run pylint and output to text and HTML
    - pylint --exit-zero --output-format=text $PY_FILES | tee /tmp/pylint.txt
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $PY_FILES > public/lint/index.html

    # Extract pylint score
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt)
    - echo $PYLINT_SCORE > public/badges/pylint.score

    # Generate codeclimate report
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter $PY_FILES > public/lint/codeclimate.json

    # Generate badge
    - anybadge --overwrite --label pylint --value=$PYLINT_SCORE --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green

    # Job will fail if the score is below 8
    - "echo 'Pylint score: $PYLINT_SCORE'"
    - |
      if (( $(echo "$PYLINT_SCORE < 8" | bc -l) )); then
        echo "Pylint score is below 8, failing the job."
        exit 1
      fi
  after_script:
    - "echo 'Pylint score: $PYLINT_SCORE'"
  artifacts:
    paths:
      - public
    reports:
      codequality: public/lint/codeclimate.json
    when: always

build_docs:
  image: authsec/sphinx
  before_script:
    - export PACKAGE_VERSION=$(git describe --tags --abbrev=0)
    - ./docs/prepare_plantuml.sh docs/src
  stage: build-docs
  script:
    - sphinx-build -b html docs public
  artifacts:
    paths:
      - public
  only:
    - main
    - tags

pages:
  stage: deploy
  dependencies:
    - build_docs
  script:
    - echo "Deploying documentation."
  artifacts:
    paths:
      - public
  only:
    - main # Ensure this is the same as in build_docs
    - tags

deploy_to_pypi:
  stage: deploy
  before_script:
    - export PACKAGE_VERSION=$(git describe --tags --abbrev=0)
    - pip install -e . # Install your package in editable mode
    - pip install -r requirements.txt # Install dependencies
  script:
    - pip install setuptools wheel twine
    - echo "__version__ = '$PACKAGE_VERSION'" > src/google_text_to_speech/_version.py
    - python setup.py sdist bdist_wheel
    - twine upload dist/* -u __token__ -p "$PYPI_API_TOKEN"
  only:
    - tags
