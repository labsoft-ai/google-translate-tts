#!/bin/bash

# Check if exactly two arguments are passed
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <path_to_project_dir> <path_to_gitignore_file>"
    exit 1
fi

project_dir="$1"
gitignore_file="$2"

# Check if project directory exists
if [ ! -d "$project_dir" ]; then
    echo "Project directory does not exist: $project_dir"
    exit 1
fi

# Check if .gitignore file exists
if [ ! -f "$gitignore_file" ]; then
    echo ".gitignore file does not exist: $gitignore_file"
    exit 1
fi

# Change to the project directory
cd "$project_dir" || exit

# Process each line in the .gitignore file
while IFS= read -r line || [[ -n "$line" ]]; do
    # Skip empty lines and comments
    if [ -z "$line" ] || [[ "$line" == \#* ]]; then
        continue
    fi

    # Handle patterns ending with a wildcard '*'
    if [[ "$line" == *"*" ]]; then
        # Remove the trailing '*' to get the directory path
        dir_path="${line%\*}"
        # Print and delete the contents of the directory
        echo "Deleting contents of: $dir_path"
        find "$dir_path" -mindepth 1 -print
        # Uncomment the next line to actually delete
        find "$dir_path" -mindepth 1 -delete
    elif [[ "$line" == *__pycache__* ]]; then
        # Special handling for __pycache__ directories
        echo "Deleting __pycache__ directories"
        find . -type d -name '__pycache__' -print
        # Uncomment the next line to actually delete
        find . -type d -name '__pycache__' -exec rm -rf {} +
    else
        # Print and delete files/directories matching .gitignore patterns
        echo "Deleting: $line"
        # Uncomment the next line to actually delete
        rm -rf "$line"
    fi
done < "$gitignore_file"

echo "Cleanup completed."
